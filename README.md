# ESC-2018 talk

## Session
### S24 Physical and Statistical models for foreshocks, aftershocks and multiplets at different scales, from laboratory experiments to real-scale observations.
#### Conveners:
 * Stefania Gentili, INOGS, Centro Ricerche Sismologiche, Italy, sgentili@inogs.it
 * Rita Di Giovambattista, INGV, Italy, rita.digiovambattista@ingv.it
 * Filippos Vallianatos, Technological Educational Institute of Crete, fvallian@chania.teicrete.gr

#### link
http://www.escmalta2018.eu/page/Sessions_n#S24

#### Schedule
**MONDAY 3RD SEPTEMBER 2018, Grima Hall, 15:45**

program: https://drive.google.com/open?id=1endf2tfDFObj6rnuwdvtoVkInmoNGQpT


## Talk
### Different patterns of aftershock sequences in three recent central Apennines earthquakes

### Authors: Giovanni Sebastiani, Aladino Govoni and Luca Pizzino

## Project management and recipes
project creation from existing draft
```
git init
git add .
git commit -m "ESC presentation - draft layout"
git remote add origin git@gitlab.rm.ingv.it:Slides/esc-2018.git
git push -u origin master
```
Markdown guide : https://guides.github.com/features/mastering-markdown/  
Basic guide: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html  
Workflows: https://www.atlassian.com/git/tutorials/comparing-workflows

## Makefile
```
make help

creates pdf presentation using latexmk
 make          : generate presentation pdf
 make pdf      : generate optimized pdf
 make verbose  : show latex compiler output
 make clean    : delete temporary files
 make cleanall : delete all temporary files and output

```
