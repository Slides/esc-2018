DOC	:= esc-2018
#BIBLIO : $(DOC).bib
LATEX	:= pdf
FLAGS	:= -quiet -bibtex
KEYWORDS:= Apenninic earthquakes, aftershock kinematics, seismic events inter-arrival times
VPATH := data/Colfiorito:data/Aquila:data/Amatrice

.PHONY : all help pdf verbose clean cleanall

all :
	latexmk -$(LATEX) $(FLAGS) $(DOC).tex

help :
	@echo ""
	@echo "creates pdf presentation using latexmk"
	@echo " make          : generate presentation pdf"
	@echo " make pdf      : generate optimized pdf"
	@echo " make verbose  : show latex compiler output"
	@echo " make clean    : delete temporary files"
	@echo " make cleanall : delete all temporary files and output"
	@echo ""

pdf : all
# GPL Ghostscript 9.05: Set UseCIEColor for UseDeviceIndependentColor to work properly.
	pdftk $(DOC).pdf dump_data output $(DOC).meta
	ghostscript \
		-sDEVICE=pdfwrite -dCompatibilityLevel=1.4\
		-dPDFSETTINGS=/printer \
		-dEmbedAllFonts=true -dSubsetFonts=true \
		-dUseCIEColor \
		-dNOPAUSE -dQUIET -dBATCH \
		-sOutputFile=$(DOC)-gs.pdf $(DOC).pdf
	@echo "InfoKey: Keywords" >> $(DOC).meta
	@echo "InfoValue: $(KEYWORDS)" >> $(DOC).meta
	#
	pdftk $(DOC)-gs.pdf update_info $(DOC).meta output $(DOC)-final.pdf
	rm -rf $(DOC)-gs.pdf $(DOC).meta
	@ls -lh $(DOC)*.pdf
#
# metadata management -- to be implemented
#1) pdftk book.pdf dump_data output report.txt
#2) edit report.txt
#3) pdftk book.pdf update_info report.txt output bookcopy.pdf
# format for metedata is
#InfoKey: Keywords
#InfoValue: Apenninic earthquakes, aftershock kinematics, seismic events inter-arrival times
#
verbose :
	latexmk -g -$(LATEX) $(FLAGS) -verbose $(DOC).tex

check:
	pdflatex $(DOC)

clean :
	latexmk -c
	rm -f *.lol *.aux *.bbl *.aux *.log *.nav *.out *.snm *.toc *.vrb *.auxlock

cleanall : clean
	latexmk -C -bibtex
	rm -rf $(DOC).pdf $(DOC)-final.pdf

# cleanup untracked files after changing .gitignore
# git ls-files --ignored --exclude-standard -z | xargs -0 git rm --cached
#
# Versions
#git tag -a malta -m "presented at ESC, Malta 2018-09-03" #did not use -a
#
commit :
	git add --all
	git commit -m "add gmt scripts"
#	git push origin master

tag:
	git tag -a malta -m "presented at ESC, Malta, 2018-09-03"

push:
	git push origin master

pull:
	git pull

# gdrive https://www.howtoforge.com/tutorial/how-to-access-google-drive-from-linux-gdrive/
# https://github.com/prasmussen/gdrive
# gdrive ID of papero is 16JkXT09IASab4xSq96qTbFLzxGwtm-Y8
# synch the papero dir command gdrive download -r  16JkXT09IASab4xSq96qTbFLzxGwtm-Y8
# created esc-2018 directory to share talk
# ID is
# gdrive:

Colfiorito-MC: OUT = figures/Colfiorito/Colfiorito-1997
Colfiorito-MC: 00_Colfiorito-1997_csi+cpti15.iside
	isideMakeMagnitudeDistribution -b 0.1 -w 0 -o $(OUT)-Mc $<
	epstopdf -o $(OUT)-Mc.pdf $(OUT)-Mc.eps
	rm -rf $(OUT)-Mc.eps

Colfiorito-EH: OUT = figures/Colfiorito/Colfiorito-1997
Colfiorito-EH: 00_Colfiorito-1997_csi+cpti15.iside
	isideTrim -s 1997-09-01 -e 1998-05-01 $< | grep -v isideTrim | grep -v type > trim.iside
	isideMakeEventHistogram -anc trim.iside
	mv trim.pdf $(OUT)-ehc.pdf
	isideMakeEventHistogram -anE trim.iside
	mv trim.pdf $(OUT)-ehe.pdf
	rm -rf trim.iside

Colfiorito-DD: OUT = figures/Colfiorito/Colfiorito-1997
Colfiorito-DD: 00_Colfiorito-1997_csi+cpti15.iside
	isideMakeDepthDistribution -b 1 -d 45 -o $(OUT)-dd $<
	epstopdf --autorotate=All -o $(OUT)-dd.pdf $(OUT)-dd.eps
	#convert -rotate 90 $(OUT)-dr.pdf $(OUT)-dd.pdf
	rm -rf $(OUT)-dd.eps

Colfiorito: Colfiorito-DD Colfiorito-EH Colfiorito-MC
	#

Aquila-EH: OUT = figures/Aquila/Aquila-2009
Aquila-EH:01_Aquila-2009.iside
	isideTrim -s 2009-03-01 -e 2010-01-01 $< | grep -v isideTrim | grep -v type > trim.iside
	isideMakeEventHistogram -anc trim.iside
	mv trim.pdf $(OUT)-ehc.pdf
	#
	isideMakeEventHistogram -anE trim.iside
	mv trim.pdf $(OUT)-ehe.pdf
	#
	rm -rf trim.iside

Aquila-MC: OUT = figures/Aquila/Aquila-2009
Aquila-MC: 01_Aquila-2009.iside
	isideMakeMagnitudeDistribution -b 0.1 -w 1 -o $(OUT)-Mc $<
	epstopdf -o $(OUT)-Mc.pdf $(OUT)-Mc.eps
	rm -rf $(OUT)-Mc.eps

Aquila-DD: OUT = figures/Aquila/Aquila-2009
Aquila-DD: 01_Aquila-2009.iside
	isideMakeDepthDistribution -b 1 -d 20 -o $(OUT)-dd $<
	epstopdf --autorotate=All -o $(OUT)-dd.pdf $(OUT)-dd.eps
	#convert -rotate 90 $(OUT)-dr.pdf $(OUT)-dd.pdf
	rm -rf $(OUT)-dd.eps

Aquila: Aquila-MC Aquila-EH Aquila-DD

Amatrice-MC: OUT = figures/Amatrice/Amatrice-2016
Amatrice-MC: 02_Amatrice-2016_2017-12-06.iside
	isideMakeMagnitudeDistribution -b 0.1 -w 2 -o $(OUT)-Mc $<
	epstopdf -o $(OUT)-Mc.pdf $(OUT)-Mc.eps
	rm -rf $(OUT)-Mc.eps

Amatrice-EH: OUT = figures/Amatrice/Amatrice-2016
Amatrice-EH: 02_Amatrice-2016_2017-12-06.iside
#	isideTrim -s 2016-03-01 -e 2010-01-01 $< | grep -v isideTrim | grep -v type > trim.iside
	isideTrim $< | grep -v isideTrim | grep -v type > trim.iside
	isideMakeEventHistogram -anc trim.iside
	mv trim.pdf $(OUT)-ehc.pdf
	#
	isideMakeEventHistogram -anE trim.iside
	mv trim.pdf $(OUT)-ehe.pdf
	#
	rm -rf trim.iside

Amatrice-DD: OUT = figures/Amatrice/Amatrice-2016
Amatrice-DD: 02_Amatrice-2016_2017-12-06.iside
	isideMakeDepthDistribution -b 1 -d 22 -o $(OUT)-dd $<
	epstopdf --autorotate=All -o $(OUT)-dd.pdf $(OUT)-dd.eps
	#convert -rotate 90 $(OUT)-dr.pdf $(OUT)-dd.pdf
	rm -rf $(OUT)-dd.eps

Amatrice: Amatrice-MC Amatrice-EH Amatrice-DD

All-DD: Amatrice-DD Aquila-DD Colfiorito-DD
